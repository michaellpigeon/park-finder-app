# NRV Parks

## Development
### Develop using Docker
From this directory, run `docker-compose up` to get started. This will create node_modules and www directories, and could take a little while
to download everything. Once it is running (watch for `dev server running: http://localhost:8100/` in the docker-compose logs), you should be able to access it at http://localhost:8080


### To run locally:
* clone the repo
* cd to the repo directory
* npm install
* npm run ionic:serve --copy copy.config.js


## Data
We use [this Google Sheet](https://docs.google.com/spreadsheets/d/15-60RpIqkM2nev4S0UNt7h5M5mhuvegPFO8X-TU8rKg/edit?usp=sharing) 
as our data curating source which feeds the data ultimately coming to the website from a PostgreSQL database via
a [REST API](https://parks.api.codefornrv.org).
