export class GoogleAnalytics {
    GoogleAnalyticsLoaded: boolean;
    Tracker: any = null;

    constructor() {
        var googleAnalyticsLoaded = typeof (<any>window).ga === 'function';
        if (googleAnalyticsLoaded) {
            if ((<any>window).ga && (<any>window).ga.getAll && (<any>window).ga.getAll()[0]) {
                this.Tracker = (<any>window).ga.getAll()[0];
            }
        }
    }

    public CollectEvent(eventCategory: string, eventAction: string, name: string) {
        if (this.Tracker !== null) {
            this.Tracker.send({
                hitType: 'event',
                eventCategory: eventCategory,
                eventAction: eventAction,
                eventLabel: name
            });
        }
    }

    public CollectPageView(pageName: string) {
        if (this.Tracker !== null) {
            this.Tracker.send({
                hitType: 'pageview',
                page: pageName
            });
        }
    }
}