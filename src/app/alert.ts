import { Events, AlertController } from 'ionic-angular';

export function EmptyResults(events: Events, alert: AlertController) {
    alert.create({
        title: 'Warning',
        subTitle: 'No search results:  reset filter?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => { }
          },
          {
            text: 'Yes',
            handler: () => {
              events.publish('filter:resetRequested');
            }
          }
        ]
      }).present();
}