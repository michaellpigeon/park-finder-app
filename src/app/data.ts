import { Amenity, JsonAmenity, JsonAmenitiesParks } from './amenity'
import { Park, JsonPark } from './park'
import { IFilter, AmenityFilter, OrFilter, SearchFilter } from './filter'
import { HttpClient } from '@angular/common/http'
import { forkJoin } from 'rxjs/observable/forkJoin'

export interface IData {
    Initialize(client: HttpClient, callback: () => void, errorCallback: () => void): void;
    GetFilters(): Array<IFilter>;
    GetAmenityFilters(): Array<IFilter>;
    GetTopFilters(): Array<IFilter>;
    FilterUpdated(): void;
    SortParks(by: string): void;
    ClearFilters(): void;
    SetUserLocation(lat: number, lon: number): void;
    GetAmenities(): Array<Amenity>;
    Search(term: string): IFilter;
    ActivePark: Park;
    Parks: Array<Park>;
    Busy: boolean;
}

abstract class BaseData implements IData {
    protected UserLocationLat: number;
    protected UserLocationLon: number;
    protected Amenities: Array<Amenity> = [];
    protected Filters: Array<[IFilter, boolean]> = [];
    public Parks: Array<Park> = [];
    public ActivePark: Park = new Park({
        Id: 0,
        Name: "No park selected",
        Amenities: []
    });
    public Busy: boolean = false;

    public abstract Initialize(client: HttpClient, callback: () => void, errorCallback: () => void);

    public GetFilters(): Array<IFilter> {
        return this.Filters.filter(x => !x[1]).map(x => x[0]);
    }

    public GetAmenityFilters(): Array<IFilter> {
        return this.GetFilters().filter(f => f instanceof AmenityFilter);
    }

    public GetTopFilters(): Array<IFilter> {
        return this.Filters.filter(x => x[1]).map(x => x[0]);
    }

    public GetAmenities(): Array<Amenity> {
        return this.Amenities;
    }

    public SortParks(by: string): void {
        switch(by) {
            case 'distance':
                this.Parks = this.Parks.sort((a,b) => (a.Distance > b.Distance ? 1 : -1));
                break;
            default:
                this.Parks = this.Parks.sort((a,b) => (a.Name > b.Name ? 1 : -1));
        }
    }

    public ClearFilters(): void {
        for(let filter of this.Filters.filter(x => x[0].IsActive).map(x => x[0])) {
            filter.IsActive = false;
        }
        this.FilterUpdated();
    }

    public Search(term: string): IFilter {
        var searchFilter = new SearchFilter(term);
        this.Filters = this.Filters.filter(f => f[0].Name !== "Search");
        this.Filters.push([searchFilter, false]);
        searchFilter.IsActive = true;
        this.FilterUpdated();
        return searchFilter;
    }

    // deactivates other filters - might change later
    public FilterUpdated() {
        // and the filters together
        var visibleParks = this.Parks;
        for(let afilter of this.Filters.filter(x => x[0].IsActive).map(x => x[0])) {
            visibleParks = visibleParks.filter(
                p => afilter.Filter(visibleParks).filter(avp => avp == p).length > 0
            );
        }
        var visibleParksSet = new Set<Park>(visibleParks);

        this.Parks.forEach(function(p) {
            p.Visible = visibleParksSet.has(p);
        });
    }

    public SetUserLocation(lat: number, lon: number): void {
        this.UserLocationLat = lat;
        this.UserLocationLon = lon;
        for (let park of this.Parks) {
            park.SetUserLocation(lat, lon);
        }
     }
}

export class APIData extends BaseData {
    private AmenityMap: { [id: number]: Amenity; } = { };
    private ParkMap: { [id: number]: Park; } = { };
    // hard-coded:  consider storing in the database eventually
    private TopAmenityFilterIds: { [id: number]: boolean; } = { 6: true, 9: true };
    // hard-coded:  consider storing in the database eventually
    private TopAmenityFilterGroups: Array<[string, Array<number>]> = [
        ["Picnic", [7, 8]],
        ["Parks with Trails", [3, 4, 5]],
        ["Sports", [1, 10, 12, 20, 22, 2, 44, 26, 49, 34, 35, 11, 50]]
    ];

    public Initialize(client: HttpClient, callback: () => void, errorCallback: () => void) {
        forkJoin(
            client.get<JsonPark[]>('https://parks.api.codefornrv.org/parks_with_geojson', { responseType: 'json' }),
            client.get<JsonAmenity[]>('https://parks.api.codefornrv.org/amenity_type', { responseType: 'json' }),
            client.get<JsonAmenitiesParks[]>('https://parks.api.codefornrv.org/amenities', { responseType: 'json' })
        ).subscribe(
            result => this.Setup(result[0], result[1], result[2], callback),
            err => errorCallback()
        );
    }

    private Setup(parksData: JsonPark[], amenityTypesData: JsonAmenity[], joins: JsonAmenitiesParks[], callback: () => void) {
        this.SetupAmenityTypes(amenityTypesData);
        this.SetupParks(parksData);
        this.SetupJoins(joins);
        this.RemoveUnusedAmenities();
        this.SetupFilters();
        this.ActivePark = this.Parks[0];
        callback();
    }

    private SetupFilters() {
        this.Filters = [];
        for (let filterSpec of this.TopAmenityFilterGroups) {
            this.Filters.push([new OrFilter(filterSpec[1].map(id => this.AmenityMap[id]), filterSpec[0]), true]);
        }
        for (let amenity of this.Amenities) {
            this.Filters.push([new AmenityFilter(amenity), amenity.Id in this.TopAmenityFilterIds]);
        }
        this.Filters = this.Filters.sort((a, b) => (a[0].Name > b[0].Name ? 1 : -1));
    }

    private SetupJoins(joins: JsonAmenitiesParks[]) {
        for (let join of joins) {
            if (join.park_id in this.ParkMap && join.amenity_type_id in this.AmenityMap) {
                this.ParkMap[join.park_id].Amenities.push(this.AmenityMap[join.amenity_type_id]);
            }
            else {
                console.log("Invalid park and/or amenity:  " + join.park_id + " / " + join.amenity_type_id);
            }
        }
    }

    private RemoveUnusedAmenities(): void {
        var AmenitiesSeen = this.Parks
            .map(p => p.Amenities)
            .reduce((a, b) => a.concat(b))
            .map(a => a.Id);
        this.Amenities = this.Amenities.filter(a => AmenitiesSeen.indexOf(a.Id) > -1);
    }

    private SetupParks(parksData: JsonPark[]) {
        for (let parkData of parksData) {
            var newPark = new Park({
                Id: parkData.id, 
                Name: parkData.park_name, 
                Amenities: [], 
                Lat: parkData.point_location.coordinates[1], 
                Lon: parkData.point_location.coordinates[0],
                AlternateNames: parkData.alternate_names,
                Description: parkData.description,
                Address: parkData.address,
                URL: parkData.url
            });
            this.Parks.push(newPark);
            this.ParkMap[newPark.Id] = newPark;
        }
    }

    private SetupAmenityTypes(amenityTypesData: JsonAmenity[]) {
        for (let amenityData of amenityTypesData) {
            var newAmenity = new Amenity({
                Id: amenityData.id, Name: amenityData.amenity_name
            });
            this.Amenities.push(newAmenity);
            this.AmenityMap[newAmenity.Id] = newAmenity;
        }
    }
}

export class MockData extends BaseData {
    public Initialize(client: HttpClient, callback: () => void, errorCallback: () => void) {
        this.Amenities = [
            new Amenity({Id: 1, Name: 'Picnic Tables'}),
            new Amenity({Id: 2, Name: 'Shelter'}),
            new Amenity({Id: 3, Name: 'Hiking'})
        ];

        var f1 = new AmenityFilter(this.Amenities[0]);
        var f2 = new AmenityFilter(this.Amenities[1]);
        var f3 = new AmenityFilter(this.Amenities[2]);

        this.Filters = [
            [new OrFilter([this.Amenities[0], this.Amenities[1]], "Picnic"), true],
            [f1, false], [f2, false], [f3, false]
        ];

        this.Parks = [
            new Park({Id: 1, Name: 'Park 1', Amenities: [this.Amenities[0]], Lat: 37.129852, Lon: -80.408939}),
            new Park({Id: 2, Name: 'Park 2', Amenities: [this.Amenities[1]], Lat: 37.130952, Lon: -80.407939}),
            new Park({Id: 3, Name: 'Park 3', Amenities: [this.Amenities[2]], Lat: 37.128752, Lon: -80.409939}),
            new Park({Id: 4, Name: 'Park 4', Amenities: [this.Amenities[0], this.Amenities[2]], Lat: 37.129852, Lon: -80.408839}),
            new Park({Id: 5, Name: 'Park 5', Amenities: [this.Amenities[1], this.Amenities[2]], Lat: 37.129852, Lon: -80.408739}),
            new Park({Id: 6, Name: 'Park 6', Amenities: [this.Amenities[0]], Lat: 37.139852, Lon: -80.418839}),
            new Park({Id: 7, Name: 'Park 7', Amenities: [this.Amenities[1]], Lat: 37.119852, Lon: -80.398739})
        ];

        this.ActivePark = this.Parks[0];
        callback();
    }
}
