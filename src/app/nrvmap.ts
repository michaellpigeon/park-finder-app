import * as L from 'leaflet';
import {Park} from './park'
import {IFilter} from './filter'

export class NRVMap {
    public Map: L.Map;
    public Layer: L.TileLayer;
    public Markers: Array<L.marker> = [];
    public UserLocation: L.marker;
    public IndexedMarkers: {[key: number]: L.marker} = {};
    public Filter: IFilter = null;

    constructor(selector: string, layerURL: string, attribution: string, lat: number, lon: number) {
        this.Map = new L.Map(selector).setView([lat, lon], 10);
        this.Map.zoomControl.setPosition('bottomright');
        this.Layer = new L.TileLayer(layerURL, { attribution: attribution });
        this.Layer.addTo(this.Map);
    }

    public ResetFocus() {
        this.Markers.forEach((m) => {
            m.setIcon(L.icon({ 
                iconUrl: 'build/images/marker-icon.png'
            }));
            m.setOpacity(1);
        });
    }

    public SetFocus(marker: L.marker) {
        this.Markers.forEach((m) => {
            m.setOpacity(0.5);
            m.setIcon(L.icon({
                iconUrl: 'assets/imgs/marker-icon-grey.png'
            }));
        });
        marker.setIcon(L.icon({ 
            iconUrl: 'build/images/marker-icon.png'
        }));
        marker.setOpacity(1);
    }

    public AddPark(park: Park) {
        this.IndexedMarkers[park.Id] = new L.marker([park.Lat, park.Lon]).addTo(this.Map).on('click',(e) => {
            document.getElementById('toggleRightMenu' + park.Id).click();
            this.SetFocus(e.target);
        });
        
        let parkDescription = '';
        if (park.Description != '') {
            parkDescription = "<small>" + park.Description + "</small>";
        }

        this.Markers.push(this.IndexedMarkers[park.Id]);
    }

    public HideMarker(parkId: number) {
        this.IndexedMarkers[parkId].remove();
    }

    public ShowMarker(parkId: number) {
        this.IndexedMarkers[parkId].addTo(this.Map);
    }

    public FitBounds() {
        var visibleMarkers = this.Markers.filter(marker => marker._map != null);
        if (visibleMarkers.length > 0) {
            let group = new L.featureGroup(visibleMarkers);
            if(typeof(this.UserLocation) !== 'undefined') {
                group.addLayer(this.UserLocation);
            }
            
            this.Map.fitBounds(group.getBounds(), {paddingTopLeft: [0, 100]});
        }
        
    }

    public SetUserLocation(position) {
        var currentIcon = L.icon({
            iconUrl: 'assets/imgs/current.svg',
            iconSize:     [38, 38]
        });

        this.UserLocation = new L.marker([position.coords.latitude, position.coords.longitude], {icon: currentIcon}).addTo(this.Map);
        this.UserLocation.setZIndexOffset(100);

        var popup = L.popup({
            autoPanPaddingTopLeft: [0, 100]
        }).setContent("<h2>You</h2>");
        
        this.UserLocation.bindPopup(popup);
        this.Map.setView([position.coords.latitude, position.coords.longitude], 14);
    }
}