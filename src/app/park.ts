import {Amenity} from './amenity'

export class Park {
    public Amenities: Array<Amenity>;
    public Name: string;
    public Lat: number;
    public Lon: number;
    public Id: number;
    public Distance: number;
    public AlternateNames: Array<string>;
    public Description: string;
    public Address: string;
    public URL: string;
    public Visible: boolean = true;
    public DirectionsURL: string;
    public DirectionsText: string = 'View on Google Maps';

    public constructor(init?:Partial<Park>) {
        Object.assign(this, init);
        if (!this.DirectionsURL) {
            if (this.Address != null && this.Address != "") {
                this.DirectionsURL = 'https://www.google.com/maps/?q=' + encodeURI(this.Address);
            } else {
                if (this.Lat != null && this.Lon != null) {
                    this.DirectionsURL = 'https://www.google.com/maps/?q=' + this.Lat + ',' + this.Lon;
                } else {
                    this.DirectionsURL = '';
                }
            }
        }
    }

    public GetTitle(): string {
        var title = this.Name;
        if (null != this.Distance) {
            title += " (" + this.Distance + " mi)";
        }
        return title;
    }

    public SetUserLocation(userLat: number, userLon: number) {
        if (this.Address == null || this.Address == "") {
            this.DirectionsURL = 'https://www.google.com/maps/dir/' + userLat + ',+' + userLon + '/' + this.Lat + ',' + this.Lon + '/';
        } else {
            this.DirectionsURL = 'https://www.google.com/maps/dir/' + userLat + ',+' + userLon + '/' + encodeURI(this.Address) + '/';
        }

        if (this.Lat != null && this.Lon != null) {
            this.Distance = Math.round(10 * this.CalculateDistance(userLat, this.Lat, userLon, this.Lon))/10;
        }

        this.DirectionsText = 'Directions on Google Maps';
    }

    // https://stackoverflow.com/questions/42724400/
    private CalculateDistance(lat1:number, lat2:number, long1:number, long2:number): number {
        let p = 0.017453292519943295;    // Math.PI / 180
        let c = Math.cos;
        let a = 0.5 - c((lat1-lat2) * p) / 2 + c(lat2 * p) *c((lat1) * p) * (1 - c(((long1- long2) * p))) / 2;
        let dis = (7918 * Math.asin(Math.sqrt(a))); // 2 * R; R = 3959 mi
        return dis;
    }
}

export interface JsonPark {
    id: number;
    park_name: string;
    point_location: JsonParkPoint;
    description: string;
    address: string;
    url: string;
    alternate_names: Array<string>;
}

export interface JsonParkPoint {
    type: string;
    coordinates: Array<number>;
}