import { Component } from '@angular/core';
import { Events, NavParams, AlertController } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';
import { NRVMap } from '../../app/nrvmap'
import { Park } from '../../app/park'
import { IData } from '../../app/data';
import { EmptyResults } from '../../app/alert';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public Map: NRVMap;
  public Data: IData;
  public SearchStr: string;

  constructor(public navParams: NavParams, platform: Platform, public events: Events, 
    private alertCtrl: AlertController) {

    events.subscribe('userlocation:updated', this.UserLocationUpdated);
    events.subscribe('filter:updated', this.FilterUpdated);
    events.subscribe('menu:closed', this.MenuClosed);
    events.subscribe('filter:reset', this.FilterReset);

    this.Data = navParams.get('data');
  }

  private UserLocationUpdated = (position: any) => {
    this.Map.SetUserLocation(position);
    this.Data.SetUserLocation(position.coords.latitude, position.coords.longitude);
  }

  private MenuClosed = () => {
    this.Map.ResetFocus();
  }

  private FilterUpdated = () => {
      this.UpdateMarkers();
      // todo: find source of asynchronicity
      setTimeout(() => this.Map.FitBounds(), 500);
  }

  private FilterReset = () => {
    this.SearchStr = "";
    this.UpdateMarkers();
    // todo: find source of asynchronicity
    setTimeout(() => this.Map.FitBounds(), 500);
  }

  public InitializeData() {

    for(let park of this.Data.Parks) {
      this.AddPark(park);
    }

    this.UpdateMarkers();

    this.Map.FitBounds();
  }

  public AddPark(park: Park) {
    this.Map.AddPark(park);
  }

  public UpdateMarkers() {
    if (this.Data.Parks.filter(p => p.Visible).length === 0) {
      EmptyResults(this.events, this.alertCtrl);
    } 

    for(let park of this.Data.Parks) {
      if(park.Visible) {
        this.Map.ShowMarker(park.Id);
      } else {
        this.Map.HideMarker(park.Id);
      }
    }
  }

  ionViewDidLoad() {
    var layerURL = 'https://api.mapbox.com/styles/v1/nealf/cj4a7oll139od2spe6ckgufjq/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibmVhbGYiLCJhIjoiNmM4MGQ3M2UzNmVlMTY0OWNmZDhiZjk0YWZlYzQ4OTYifQ.VEiV66Tl7sjD5n-bDLjbhw';
    var attribution = '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
    this.Map = new NRVMap('mapid', layerURL, attribution, 37.129852, -80.408939);
    this.InitializeData();
  }

  ionViewWillLeave() {
    this.events.unsubscribe('userlocation:updated', this.UserLocationUpdated);
    this.events.unsubscribe('filter:updated', this.FilterUpdated);
    this.events.unsubscribe('filter:reset', this.FilterReset);
    this.events.unsubscribe('menu:closed', this.MenuClosed);
  }

  public SetActivePark(park: Park) {
    this.Data.ActivePark = park;
    this.events.publish('park:selected', park);
  }

  public Search() {
    var searchStr = typeof(this.SearchStr) === 'undefined' ? "" : this.SearchStr;
    var filter = this.Data.Search(searchStr);
    this.events.publish('filter:updated', filter);
  }

  public ClearSearch() {
    this.SearchStr = "";
    this.Search();
  }
}
